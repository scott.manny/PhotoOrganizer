name := "PhotoOrganizer"

version := "0.1"

scalaVersion := "2.12.3"

libraryDependencies ++= Seq(
  "com.github.pathikrit"  %% "better-files-akka"  % "3.0.0",
  "com.typesafe.akka"     %% "akka-actor"         % "2.5.3",
  "commons-io" % "commons-io" % "2.5"
)
