import java.io.{File => JFile}
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{Files, Path}
import java.text.SimpleDateFormat
import java.util.Date

import better.files._

object PhotoOrganizerSupervisor {

  val sourcePath = "D:\\Pictures\\_ToBeFiled1"
  val destPaths = List("D:\\Pictures", "C:\\Users\\Scott\\OneDrive\\Pictures")

  def main(args: Array[String]): Unit = {
    println("Getting files...")

    val files: Seq[File] = getFiles(sourcePath).sortBy(_.name)
    println(s"Found ${files.length} files.")

    files.foreach { file =>
      val attr: BasicFileAttributes = Files.readAttributes(file.path, classOf[BasicFileAttributes])
      val year = getYear(attr, new SimpleDateFormat("yyyy"))
      val destFilename = file.name.replaceAll("\\(.*?\\) ?", "").replaceAll(" ", "").trim
      destPaths.foreach{ path =>
        val move = path == destPaths.last
        processFile(file, year, path, destFilename, move)
      }
    }
  }

  private def getFiles(dir: String): List[File] = {
    val d = dir.toFile
    if (d.exists && d.isDirectory) {
      d.listRecursively.filter(!_.isDirectory).toList
    } else {
      List[File]()
    }
  }

  private def getYear(attr: BasicFileAttributes, df: SimpleDateFormat) = {
    val lastModifiedYear = df.format(new Date(attr.lastModifiedTime().toMillis))
    val creationYear = df.format(new Date(attr.creationTime().toMillis))
    if (lastModifiedYear < creationYear) lastModifiedYear else creationYear
  }

  private def processFile(file: File, year: String, path: String, destFilename: String, move: Boolean) = {
    val destDirectory = s"$path\\$year".toFile.createIfNotExists(true)
    moveFile(file, destDirectory, destFilename, move)
  }

  private def moveFile(file: File, destDirectory: File, cleanFilename: String, move: Boolean) = {
    val destFile = s"$destDirectory\\$cleanFilename".toFile

    Files.exists(destFile.path) match {
      case true =>
        destFile.size == file.size match {
          case true => deleteFile(file, move)
          case false => {
            val modifiedFilename = randomizeFilename(cleanFilename)
            val modifiedFile = s"$destDirectory\\$modifiedFilename".toFile
            moveIt(file, modifiedFile, move)
          }
        }
      case false => moveIt(file, destFile, move)
    }
  }

  private def moveIt(file: File, modifiedFile: File, move: Boolean) = {
    move match {
      case true => {
        logFileMove(modifiedFile, "moving")
        Files.move(file.path, modifiedFile.path)
      }
      case false => {
        logFileMove(modifiedFile, "copying")
        Files.copy(file.path, modifiedFile.path)
      }
    }
  }

  private def deleteFile(file: File, delete: Boolean) = {
    delete match {
      case true => {
        Files.delete(file.path)
        logFileDelete(file, "deleting")
      }
      case false => {
        logFileDelete(file, "skipping")
      }
    }
  }

  private def logFileDelete(file: File, verb: String) = {
    println(s"=== $verb ${file.name}")
  }

  private def logFileMove(destFile: File, verb: String) = {
    println(s"*** $verb to ${destFile.path}")
  }

  private def randomizeFilename(filename: String) = {
    val nameOnly = filename.split('.')(0)
    val extension = filename.split('.')(1)
    nameOnly + randomAlphaNumericString(5) + "." + extension
  }

  private def randomAlphaNumericString(length: Int): String = {
    val chars = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')
    randomStringFromCharList(length, chars)
  }

  private def randomStringFromCharList(length: Int, chars: Seq[Char]): String = {
    val sb = new StringBuilder
    for (i <- 1 to length) {
      val randomNum = util.Random.nextInt(chars.length)
      sb.append(chars(randomNum))
    }
    sb.toString
  }
}